# Preview all emails at http://localhost:3000/rails/mailers/message_mailer
class MessageMailerPreview < ActionMailer::Preview
  def contact_message_preview
    MessageMailer.contact_message(Message.first)
  end
end
