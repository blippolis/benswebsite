class  Message < ApplicationRecord
	validates :name, :email, :subject, :body, :presence => true
	validates :email, :format => { :with => %r{.+@.+\..+} }, :allow_blank => true
end
