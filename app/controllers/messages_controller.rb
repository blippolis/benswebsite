class MessagesController < ApplicationController

  def new
    @message = Message.new
  end

  def create
    @message = Message.create(message_params)

    if @message.save
      MessageMailer.contact_message(@message).deliver_later
      flash[:success] = "Success! You sent me a message!"
      redirect_to root_path
    else
      flash[:danger] = "Message failed to send. Please fill out all fields."
      redirect_to root_path 
    end

  end
  
  private 

  def message_params
    params.require(:message).permit(:name, :email, :subject, :body)
  end
end