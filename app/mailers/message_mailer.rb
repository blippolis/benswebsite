class MessageMailer < ApplicationMailer
    default from: "lippolis.b@husky.neu.edu"

    def contact_message(message)
        @message = message
        mail(to: "lippolis.b@husky.neu.edu", subject: "Contact form message")
    end
end
